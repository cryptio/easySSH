#include <iostream>
#include <libssh/libsshpp.hpp>
#include "easySSH.h"

int main(int argc, char *argv[]) {
    ssh::Session my_session;

    if (argc != 5) {
        std::cerr << "Wrong number of arguments\n";
        return 1;
    }

    const std::string user = argv[1];
    const std::string host = argv[2];
    const unsigned port = std::stoi(argv[3]);
    const std::string password = argv[4];

    const easySSH::Configuration my_configuration(user, host, port, password);

    connect(my_session, my_configuration);
    std::cout << "\"" << easySSH::exec(my_session, "ls -a") << "\"\n";

    return 0;
}


