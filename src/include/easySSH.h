#ifndef EASYSSH_EASYSSH_H
#define EASYSSH_EASYSSH_H

#include <string>
#include "libssh/libsshpp.hpp"

namespace easySSH {
    class Configuration {
    public:
        Configuration(const std::string &user, const std::string &host, const unsigned &port,
                      const std::string &password);

        const std::string &get_user() const;

        const std::string &get_host() const;

        const unsigned &get_port() const;

        const std::string &get_password() const;

        void set_user(const std::string &user);

        void set_host(const std::string &host);

        void set_port(const unsigned &port);

    private:
        std::string user_, host_, password_;
        unsigned port_;
    };

    /**
    * @brief Executes a remote command over an existing SSH session
    * @param session The session to execute a command on
    * @param cmd The command to execute
    * @return Output of the command
    */
    std::string exec(ssh::Session &session, const std::string &cmd);

    /**
     * @brief Opens an SSH session using the provided information
     * @param session The session to use for the connection (libssh doesn't allow assignment/copying of Sessions)
     * @param user  The username
     * @param host  The remote host to connect to
     * @param port  The port to connect to
     * @param password  The password to authenticate with
     * @note Since libssh's Session class has its copy constructor and assignment operator private, we need to pass a non-const
     * reference to the target Session. After connect() has finished, the Session object will be connected given the specified
     * details.
     */
    void connect(ssh::Session &session, const std::string &user, const std::string &host, const unsigned &port,
                 const std::string &password);

    /**
     * @brief Opens an SSH session using the provided information
     * @param session The session to use for the connection
     * @param configuration Holds the configuration for the target Session
     * @note Since libssh's Session class has its copy constructor and assignment operator private, we need to pass a non-const
     * reference to the target Session. After connect() has finished, the Session object will be connected given the specified
     * details.
     */
    void connect(ssh::Session &session, const easySSH::Configuration &configuration);
}


#endif //EASYSSH_EASYSSH_H
