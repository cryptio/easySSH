#include <iostream>
#include "easySSH.h"

easySSH::Configuration::Configuration(const std::string &user, const std::string &host, const unsigned &port,
                                      const std::string &password)
        : user_(user), host_(host), port_(port), password_(password) {

}

const std::string &easySSH::Configuration::get_user() const {
    return Configuration::user_;
}

const std::string &easySSH::Configuration::get_host() const {
    return Configuration::host_;
}

const std::string &easySSH::Configuration::get_password() const {
    return Configuration::password_;
}

const unsigned &easySSH::Configuration::get_port() const {
    return Configuration::port_;
}

void easySSH::Configuration::set_user(const std::string &user) {
    user_ = user_;
}

void easySSH::Configuration::set_host(const std::string &host) {
    host_ = host;
}

void easySSH::Configuration::set_port(const unsigned &port) {
    bool port_valid = (port > 0 && port < 65535);
    if (port_valid) {
        port_ = port;
    } else {
        throw std::invalid_argument("Invalid port number\n");
    }
}

std::string easySSH::exec(ssh::Session &session, const std::string &cmd) {
    ssh::Channel channel(session);
    char buffer[256];
    try {
        channel.openSession();
        channel.requestExec(cmd.c_str());   // send the command over the channel
        channel.read(buffer, sizeof(buffer));   // read contents of channel into buffer
        channel.sendEof();
        channel.close();    // close the channel
    } catch (ssh::SshException &error) {
        std::cerr << "Error code " << error.getCode() << ": " << error.getError() << '\n';
        return "";
    }

    std::string output = buffer;
    return output;
}


void easySSH::connect(ssh::Session &session, const std::string &user, const std::string &host, const unsigned &port,
             const std::string &password) {
    //ssh::Session session;
    const auto verbosity = SSH_LOG_PROTOCOL;    // log high level protocol information
    try {
        session.setOption(SSH_OPTIONS_LOG_VERBOSITY, verbosity);
        session.setOption(SSH_OPTIONS_USER, user.c_str());
        session.setOption(SSH_OPTIONS_HOST, host.c_str());
        session.setOption(SSH_OPTIONS_PORT, port);

        session.connect();
        const bool serverKnown = session.isServerKnown() == SSH_SERVER_KNOWN_OK;
        switch (serverKnown) {
            case false: {
                const bool writeKnownhost_success = session.writeKnownhost() == SSH_OK;
                if (writeKnownhost_success) {
                    session.connect();
                } else {
                    std::cerr << "writeKnownhost failed\n";
                }
                break;
            }
            default:
                break;
        }
        // try to automatically find and authenticate using a public key
        const bool auth_success_Publickey = (session.userauthPublickeyAuto() == SSH_AUTH_SUCCESS);
        if (auth_success_Publickey) {
            std::cout << "Public key auth succeeded\n";
        } else {
            std::cerr << "Public key auth failed\n";
            // attempt to authenticate with the provided password
            const bool auth_success = (session.userauthPassword(password.c_str()) == SSH_AUTH_SUCCESS);
            if (!auth_success) {
                std::cerr << "Password auth failed\n";
            }
        }

    } catch (ssh::SshException &error) {
        std::cerr << "Error code " << error.getCode() << ": " << error.getError() << '\n';
    }
}

void easySSH::connect(ssh::Session &session, const easySSH::Configuration &configuration) {
    const std::string user = configuration.get_user();
    const std::string host = configuration.get_host();
    const unsigned port = configuration.get_port();
    const std::string password = configuration.get_password();

    connect(session, user, host, port, password);
}